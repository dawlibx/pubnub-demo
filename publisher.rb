require_relative 'client'

Client.new.pubnub.publish(
    channel: 'test_789',
    message: { text: "Hi! Message from PUBLISHER! (╭☞■∀■)╭☞ #{Time.now}" }
) do |envelope|
    puts envelope.status
end

# PubNub demo app

## To run this code:

In one terminal window: `ruby subscriber.rb`
In another one: `ruby publisher.rb`

(it's necessary to run it in separate processes)

require 'pubnub'
require 'yaml'
require 'pry'

class Client
  attr_reader :pubnub

  CREDENTIALS = YAML.load_file('credentials.yml')['pubnub']

  def initialize
    @pubnub = Pubnub.new(
      publish_key: CREDENTIALS['publish_key'],
      subscribe_key: CREDENTIALS['subscribe_key']
    )

    setup_listener
  end

  def setup_listener
    callback = Pubnub::SubscribeCallback.new(
      message:  ->(envelope) {
          puts "MESSAGE: #{envelope.result[:data]} ᕙ(૦ઁ_૦ઁ)ᕗ"
      }
    )

    pubnub.add_listener(callback: callback)
  end
end
